STM32 boot loader
=================
_Tools for loading programs on to STM32 microcontrollers._

[TOC]

Supported boot modes
--------------------

* STM32 UART boot loader. This runs via a USB->UART bridge. An mbed program for this bridge is included in the repository.

Usage instructions
------------------

Run the `stm32fboot.py` program with the path to UART device. 
This program has the following options:

````
usage: stm32fboot.py [-h] [-b BAUDRATE] [-p PROGRAM] [-r READ] [-e] path

positional arguments:
  path

optional arguments:
  -h, --help            show this help message and exit
  -b BAUDRATE, --baudrate BAUDRATE
  -p PROGRAM, --program PROGRAM
  -r READ, --read READ
  -e, --erase
````


To program a device provide the `-p/--program` option with the path to the program binary. For example:

````
$ python stm32fboot.py -p test.bin /dev/ttyACM0
````

Note that you may need to set up your permissions correctly to use the serial interface. For example, on most linux distributions this can be done by adding yourself to the `dialout` group.

Tested microcontrollers
-----------------------

| uC | Programming method | Status |
|----|--------------------|--------|
| STM32F030? | UART via other uC | Success |
| STM32F103RB | UART via other uC | Success |

Dependencies
------------

These tools use Python 2.X (any recent version should work). They require pyserial:

````
pip install pyserial
````

License
-------

Copyright 2016 Nick Kolpin (nick.kolpin@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
