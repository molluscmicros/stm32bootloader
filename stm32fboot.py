"""
STM32F boot loader library.

This library provides software to use STM32F boot loaders by various interfaces.
Currently implemented are:

* UART - interfaced via a USB serial connection

-------------------------------------------------------------------------------
Copyright 2016 Nick Kolpin (nick.kolpin@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-------------------------------------------------------------------------------
"""

import argparse
import intelhex
import operator
import serial
import time


ack = 0x79
nack = 0x1f

stm32fproducts = {
    0x444: "STM32F03xx4/6",
    0x445: "STM32F04xxx",
    0x410: "STM32F10x (Medium density)"
}

class UARTBootLoader:

    def __init__(self, fn, baudrate=57600, verbose=False):
        self.ser = serial.Serial(fn, baudrate=baudrate)
        self.version = None
        self.ID = None
        self.commands = []
        self.write(0x7f, nochecksum=True, verbose=verbose)
        self.checkack(self.read())
        self.getcommand()

    def __repr__(self):
        s = ""
        if self.ID not in stm32fproducts:
            s += "Unknown chip "
            if self.ID is not None:
                s += "PID = 0x%x " % self.ID
        else:
            s += "%s " % stm32fproducts[self.ID]
        if self.version is not None:
            s += "running UART boot loader v%g " % self.version
        return s

    def checkack(self, val):
        msg = "Expected ack (0x%x), received 0x%x"
        if val == nack:
            msg = "Expected ack (0x%x), received nack (0x%x)"

        assert val == ack, msg % (ack, val)

    def write(self, x, nochecksum=False, verbose=False):
        if not x.__class__ == list:
            x = [x]
        if not nochecksum:
            x.append(self.checksum(x, verbose=verbose))
        vals = bytearray(x)
        if verbose:
            print "Writing: ", vals
        self.ser.write(vals)
        
    def read(self):
        return ord(self.ser.read())

    def readn(self, n):
        vals = []
        for i in range(n):
            vals.append(ord(self.ser.read()))
        return vals

    def checksum(self, data, verbose=False):
        if data.__class__ != list:
            return 0xff - data
        else:
            if len(data) == 1:
                checksum = 0xff - data[0]
                if verbose:
                    print "One byte checksum = 0x%x" % checksum
                return checksum
            if verbose:
                print "Calaculating xor checksum for %s" % str(data)
            checksum = 0x0
            for val in data:
                checksum = operator.xor(checksum, val)
            if verbose:
                print "Checksum = 0x%x" % checksum
            return checksum

    def address(self, addr):
        a = []
        for i in range(4):
            shift = (3 - i) * 8
            b = addr & (0xff << shift)
            b = b >> shift
            a.append(b)
        return a

    def getcommand(self):
        data = [0x0]
        self.write(data)
        vals = self.readn(2)
        self.checkack(vals[0])
        nbytes = vals[1] + 2 # n + 1 + ack
        vals = self.readn(nbytes)
        assert vals[-1] == ack
        self.version = vals[0] * 0.1
        for i in range(1, nbytes - 1):
            self.commands.append(vals[i])

    def getversion(self):
        cmd = 0x1
        assert cmd in self.commands
        data = [cmd]
        self.write(data)
        vals = self.readn(5)
        self.checkack(vals[0])
        self.version = vals[1] * 0.1
        self.checkack(vals[4])

    def getID(self):
        cmd = 0x2
        assert cmd in self.commands
        data = [cmd]
        self.write(data)
        vals = self.readn(2)
        self.checkack(vals[0])
        nbytes = vals[1] + 2
        vals = self.readn(nbytes)
        self.checkack(vals[-1])
        self.ID = 0x0
        nbytes -= 1
        for i in range(nbytes):
            self.ID |= vals[i] << (nbytes -1 - i) * 8

    def readmemory(self, addr, nbytes):
        alldata = []
        cmd = 0x11
        assert cmd in self.commands
        while nbytes > 0:
            if nbytes > 256:
                ntoread = 256
            else:
                ntoread = nbytes
            nbytes -= ntoread
            data = [cmd]
            self.write(data)
            self.checkack(self.read())
            data = self.address(addr)
            self.write(data)
            self.checkack(self.read())
            data = [ntoread - 1]
            self.write(data)
            self.checkack(self.read())
            alldata.extend(self.readn(ntoread))
        return bytearray(alldata)

    def go(self, addr):
        cmd = 0x21
        assert cmd in self.commands
        data = [cmd]
        self.write(data)
        self.checkack(self.read())
        data = self.address(addr)
        self.write(data)
        self.checkack(self.read())

    def writememory(self, addr, data):
        start = time.time()
        cmd = 0x31
        assert cmd in self.commands
        nwritten = 0.0
        ntotal = len(data)
        target = 10.0
        while len(data) > 0:
            cmddat = [cmd]
            self.write(cmddat)
            self.checkack(self.read())
            a = self.address(addr)
            #msg = "Writing memory to 0x"
            #for i in range(len(a)):
            #    msg += "%02x" % a[i]
            #print msg
            self.write(a)
            self.checkack(self.read())
            nbytes = len(data)
            if nbytes > 128:
                nbytes = 128
            tosend = [nbytes - 1]
            tosend.extend(data[:nbytes])
            #print "Sending %d bytes, len = %d..." % (nbytes, len(tosend))
            self.write(tosend)
            #print "Sent. Waiting for ack/nack..."
            self.checkack(self.read())
            nwritten += nbytes
            pct = nwritten / ntotal * 100.0
            if pct >= target:
                print "%d of %d = %g%%" % (nwritten, ntotal, nwritten / ntotal * 100.0)
                target += 10.0
            data = data[nbytes:]
            addr += nbytes
        end = time.time()
        print "Wrote %d bytes at %g kB/s" % (nwritten, nwritten / (end - start) / 1000.0)

    def erasememory(self, addr, pages=[], globalerase=False):
        cmd = 0x43
        assert cmd in self.commands
        if globalerase:
            data = [cmd]
            self.write(data)
            self.checkack(self.read())
            self.write([0xff])
            self.checkack(self.read())
            return
        while len(pages) > 0:
            self.write([cmd])
            self.checkack(self.read())
            npages = len(pages)
            if npages > 254:
                npages = 254
            data = [npages - 1]
            data.extend(npages[:npages])
            self.write(data)
            self.checkack(self.read())
            pages = pages[npages:]

    def extendederasememory(self, pages=[], globalerase=False, 
                            bank1erase=False, bank2erase=False):
        cmd = 0x44
        assert cmd in self.commands
        if globalerase:
            self.write([cmd])
            self.checkack(self.read())
            self.write([0xff, 0xff])
            self.checkack(self.read())
            return
        if bank1erase:
            self.write([cmd])
            self.checkack(self.read())
            self.write([0xff, 0xfe])
            self.checkack(self.read())
        if bank1erase:
            self.write([cmd])
            self.checkack(self.read())
            self.write([0xff, 0xfd])
            self.checkack(self.read())
        if bank1erase or bank2erase:
            return
        while len(pages) > 0:
            self.write([cmd])
            self.checkack(self.read())
            npages = len(pages)
            if npages > 0xfff0:
                npages = 0xfff0
            data = [(npages & 0xff00) >> 8, (npages - 1) & 0xff]
            for i in range(npages):
                data.append((pages[i] & 0xff00) >> 8)
                data.append(pages[i] & 0xff)
            self.write(data)
            self.checkack(self.read())
            pages = pages[npages:]

    def writeprotect(self, sectors):
        cmd = 0x63
        assert cmd in self.commands
        while len(sectors) > 0:
            self.write([cmd])
            self.checkack(self.read())
            nsectors = len(sectors)
            if nsectors > 256:
                nsectors = 256
            data = [nsectors - 1]
            data.extend(sectors[nsectors])
            self.write(data)
            self.checkack(self.read())
            sectors = sectors[nsectors:]

    def writeunprotect(self):
        cmd = 0x73
        assert cmd in self.commands
        self.write([cmd])
        self.checkack(self.read())
        self.checkack(self.read())

    def readoutprotect(self):
        cmd = 0x82
        assert cmd in self.commands
        self.write([cmd])
        self.checkack(self.read())
        self.checkack(self.read())

    def readoutunprotect(self):
        cmd = 0x92
        assert cmd in self.commands
        self.write([cmd])
        self.checkack(self.read())
        self.checkack(self.read())

    def program(self, fn, addr=0x08000000):
        inp = intelhex.IntelHex()
        inp.loadbin(fn)
        data = inp.tobinarray()
        #inp = open(fn, "rb")
        #data = []
        #nextbyte = inp.read(1)
        #while nextbyte != b"":
        #    nextbyte = ord(nextbyte)
        #    #print "nextbyte = ", nextbyte, nextbyte.__class__
        #    data.append(nextbyte)
        #    nextbyte = inp.read(1)
        #inp.close()
        extra = len(data) % 4
        for i in range(extra):
            data.append(0x0)
        print "%d bytes to write." % len(data)
        #print "Removing write protection..."
        #self.writeunprotect()
        #time.sleep(1.0)
        #print "Erasing flash memory..."
        #self.extendederasememory(globalerase=True)
        #time.sleep(1.0)
        if 0x44 in self.commands:
            print "Global erase of flash..."
            self.extendederasememory([], True)
        else:
            print "Skipping memory erase..."
            print "Basic erase of memory..."
            self.erasememory(0, globalerase=True)

        print "Writing program to flash memory..."
        self.writememory(addr, data)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("path")
    parser.add_argument("-b", "--baudrate", type=int, default=57600)
    parser.add_argument("-p", "--program")
    parser.add_argument("-r", "--read", type=int, default=-1)
    parser.add_argument("-e", "--erase", action="store_true")
    args = parser.parse_args()

    bl = UARTBootLoader(args.path, args.baudrate)
    bl.getversion()
    bl.getID()
    print bl
    #print bl.commands
    if args.erase:
        print bl.commands
        print "Doing a global erase."
        bl.extendederasememory([], True)
    if args.program is not None:
        print "Programming %s" % args.program
        bl.program(args.program)
        print "Done."
    if args.read > 0:
        data = bl.readmemory(0x08000000, args.read)
        datastring = ""
        for datum in data:
            datastring += "%x" % datum
        print datastring
        outp = open("bytesread.dat", "wb")
        outp.write(data)
        outp.close()
