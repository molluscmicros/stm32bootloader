import argparse
import array
import intelhex
import struct
import sys
import time
import usb.core
import usb.core

DFU_DETATCH = 0x00
DFU_DNLOAD = 0x01
DFU_UPLOAD = 0x02
DFU_GETSTATUS = 0x03
DFU_CLRSTATUS = 0x04
DFU_GETSTATE = 0x05
DFU_ABORT = 0x06

# Status values
dfuAPPIDLE = 0x00
dfuAPPDETACH = 0x01
dfuIDLE = 0x02
dfuDOWNLOADSYNC = 0x03
dfuDOWNLOADBUSY = 0x04
dfuDOWNLOADIDLE = 0x05
dfuMANIFESTSYNC = 0x06
dfuMANIFEST = 0x07
dfuMANIFESTWAITRESET = 0x08
dfuUPLOADIDLE = 0x09
dfuERROR = 0x0a

statuses = [
    "APP_IDLE", "APP_DETATCH", "IDLE", "DOWNLOAD_SYNC", "DOWNLOAD_BUSY",
    "DOWNLOAD_IDLE", "MANIFEST_SYNC", "MANIFEST", "MANIFEST_WAIT_RESET",
    "UPLOAD_IDLE", "ERROR"
]

reqtype = 0x0
reqtype |= 0x1 # interface is recipient
reqtype |= 0x2 << 4 # vendor request type
reqtypeout = (0x0 << 7) | reqtype
reqtypein = (0x1 << 7) | reqtype
print "reqtypein = 0x%02x, reqtypeout = 0x%02x" % (reqtypein, reqtypeout)

class USBBootLoader:

    def __init__(self):
        self.vid = 0x0483
        self.pid = 0xdf11
        self.nwrite = 256
        self.timeout = 2000 # ms
        self.dev = usb.core.find(idVendor=self.vid, idProduct=self.pid)
        assert self.dev is not None
        usb.util.claim_interface(self.dev, 0x0)
        self.clearstatus()

    def transfer(self, reqtype, req, wvalue, windex, wlenordata, verbose=False):
        rep = None
        worked = False
        while not worked:
            try:
                rep = self.dev.ctrl_transfer(reqtype, req, wvalue, windex, wlenordata, self.timeout)
                worked = True
            except usb.core.USBError as e:
                if verbose:
                    print "Caught: %s" % (str(e))
        return rep

    def getcommands(self):
        req = DFU_UPLOAD
        wvalue = 0x0
        windex = 0x0
        wlength = 4
        cmds = self.dev.ctrl_transfer(reqtypein, req, wvalue, windex, wlength, self.timeout)
        print "Found commands: %s" % str(cmds)
        status = self.getstatus()
        print "Status = ", status

    def getstatus(self):
        request = DFU_GETSTATUS
        wvalue = 0x0
        windex = 0x0
        wlength = 6
        stat = self.transfer(reqtypein, request, wvalue, windex, wlength)
        #print "stat = ", stat
        return stat[4] # Why?

    def clearstatus(self):
        req = DFU_CLRSTATUS
        wvalue = 0
        windex = 0
        data = None
        self.dev.ctrl_transfer(reqtypeout, req, wvalue, windex, data, self.timeout)

    def erasepage(self, addr):
        addr &= 0xffffffff
        #print "Erasing page at 0x%08x" % addr
        req = DFU_DNLOAD
        wvalue = 0
        windex = 0
        data = array.array("B", [0x41])
        for i in range(4):
            val = (addr & (0xff << (i * 8))) >> (i * 8)
            data.append(val)
        self.transfer(reqtypeout, DFU_DNLOAD, wvalue, windex, data)
        stat = self.getstatus()
        assert stat == dfuDOWNLOADBUSY, "Error doing page (0x%08x) erase: 0x%x [expect busy = 0x02x]" % (addr, stat, dfuDOWNLOADBUSY)
        time.sleep(0.1)
        stat = self.getstatus()
        while stat == dfuDOWNLOADBUSY:
            time.sleep(0.1)
            stat = self.getstatus()
        assert stat == dfuDOWNLOADIDLE, "Error doing page (0x%08x) erase: 0x%x [expect idle = 0x%02x]" % (addr, stat , dfuDOWNLOADIDLE)

    def masserase(self):
        req = DFU_DNLOAD
        wvalue = 0
        windex = 0
        data = array.array("B", [0x41])
        self.dev.ctrl_transfer(reqtypeout, DFU_DNLOAD, wvalue, windex, data, self.timeout)
        stat = self.getstatus()
        assert stat == dfuDOWNLOADBUSY, "Error doing mass erase: 0x%x [expect busy = 0x02x]" % (stat, dfuDOWNLOADBUSY)
        time.sleep(1.0)
        stat = self.getstatus()
        if stat != dfuDOWNLOADIDLE:
            time.sleep(1.0)
            stat = self.getstatus()
        assert stat == dfuDOWNLOADIDLE, "Error doing mass erase: 0x%x [expect idle = 0x%02x]" % (stat , dfuDOWNLOADIDLE)

    def setpointer(self, addr):
        addr &= 0xffffffff
        #reqtype = deviceToHost
        req = DFU_DNLOAD
        wvalue = 0
        windex = 0
        #data = array.array("B", struct.pack("<BI", 0x21, addr))
        data = array.array("B")
        data.append(0x21)
        for i in range(4):
            val = (addr & (0xff << (i * 8))) >> (i * 8)
            data.append(val)
        #print "setpointer() data = ", data
        self.transfer(reqtypeout, DFU_DNLOAD, wvalue, windex, data)
        stat = self.getstatus()
        assert stat == dfuDOWNLOADBUSY, "Error setting pointer: 0x%x [expect busy = 0x%02x]" % (stat, dfuDOWNLOADBUSY)
        while stat == dfuDOWNLOADBUSY:
            stat = self.getstatus()
        assert stat == dfuDOWNLOADIDLE, "Error setting pointer: 0x%x [expect idle = 0x%02x]" % (stat , dfuDOWNLOADIDLE)
    
    def write(self, addr, data):
        left = False
        totallen = float(len(data))
        target = 0.1
        first = True
        while len(data) > 0:
            self.setpointer(addr)
            if 1 - len(data) / totallen > target:
                print "write %d %%" % (target * 100)
                target += 0.1
            req = DFU_DNLOAD
            wvalue = 2
            windex = 0
            transfersize = min(len(data), self.nwrite)
            addr += transfersize
            transfer = data[:transfersize]
            data = data[transfersize:]
            stat = self.getstatus()
            assert stat == dfuDOWNLOADIDLE or stat == dfuIDLE
            self.transfer(reqtypeout, req, wvalue, windex, transfer)
            stat = self.getstatus()
            assert stat == dfuDOWNLOADBUSY
            while stat == dfuDOWNLOADBUSY:
                stat = self.getstatus()
            assert stat == dfuDOWNLOADIDLE

    def leavedfu(self, addr):
        self.setpointer(addr)
        req = DFU_DNLOAD
        wvalue = 2
        windex = 0
        transfer = []
        stat = self.getstatus()
        assert stat == dfuDOWNLOADIDLE or stat == dfuIDLE
        self.dev.ctrl_transfer(reqtypeout, req, wvalue, windex, transfer, self.timeout)
        try:
            stat = self.getstatus()
            print stat, statuses[stat]
        except usb.core.USBError:
            print "USB error getting status after trying to leave DFU."


    def writefile(self, addr, fn):
        inp = intelhex.IntelHex()
        inp.loadbin(fn)
        data = inp.tobinarray()
        self.write(addr, data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    parser.add_argument("-e", "--erase", action="store_true")
    parser.add_argument("-c", "--clear", action="store_true")
    args = parser.parse_args()

    u = USBBootLoader()
    if args.clear:
        u.clearstatus()
        stat = u.getstatus()
        print "status: %d: %s" % (stat, statuses[stat])
        sys.exit()
    if args.erase:
        pages = []
        for i in range(1536):
            pages.append(0x08000000 + i * 128)
        print "Erasing pages..."
        npages = float(len(pages))
        target = 0.1
        for ipage in range(len(pages)):
            page = pages[i]
            u.erasepage(page)
            if ipage / npages >= target:
                print "%d %% complete..." % (target * 100)
                target += 0.1
        print "Erase done."
    u.writefile(0x08000000, args.filename)
    u.leavedfu(0x08000000)
